.MODEL SMALL
.CODE
org 100h
start:
    jmp mulai
    
pesanan        db 13,10,'Masukkan no meja anda : $'
lanjut          db 13,10,'TEKAN (y) untuk melanjutkan : $'
input_pesanan   db 13,?,13 dup(?)
    

daftar  db 13,10,'-------------------------------------------'
        db 13,10,'-----------Menu Makanan--------------------'
        db 13,10,'-------------------------------------------'
        db 13,10,'|01|Ayam Goreng    | | Rp 8.000  |         '
        db 13,10,'|02|Ayam Bakar     | | Rp 10.000 |         '
        db 13,10,'|03|Nasi Goreng    | | Rp 10.000 |         '
        db 13,10,'|04|Sate           | | Rp 9.000  |         '
        db 13,10,'|05|Mi Goreng      | | Rp 10.000 |         '
        db 13,10,'|06|Kembang Tahu   | | Rp 7.000  |         '
        db 13,10,'|07|Rendang        | | Rp 12.000 |         '
        db 13,10,'|08|Gado-Gado      | | Rp 8.000  |         '
        db 13,10,'|09|Sup Buntut     | | Rp 10.000 |         '
        db 13,10,'|10|Soto Ayam      | | Rp 8.000  |         '     
        db 13,10,'===========================================','$' 
        
error           db 13,10,'INPUT SALAH! $'
input_daftar    db 13,10,'Masukkan no daftar yang dipilih : $'
succes          db 13,10,'SELAMAT ANDA BERHASIL'
        

        
mulai:
    mov ah,09h
    lea dx,pesanan
    int 21h
    mov ah,0ah
    lea dx,input_pesanan
    int 21h
    push dx      
        
    
    mov ah,09h
    mov dx,offset daftar
    int 21h
    mov ah,09h
    lea dx,lanjut
    int 21h
    mov ah,01h
    int 21h
    cmp al,'y'
    je proses
    jne error_msg  
    
    
    
;-------------------------------------------------------------

error_msg:
    mov ah,09h
    mov dx,offset error
    int 21h
    
    int 20h
    
;-------------------------------------------------------------    

proses:
    mov ah,09h
    mov dx,offset input_daftar
    int 21h
    
    mov ah,1
    int 21h
    mov bh,al
    mov ah,1
    int 21h
    mov bl,al
    
    cmp bh,'0'
    cmp bl,'1'
    je hasil1
    
    cmp bh,'0'
    cmp bl,'2'
    je hasil2
    
    cmp bh,'0'
    cmp bl,'3'
    je hasil3
    
    cmp bh,'0'
    cmp bl,'4'
    je hasil4
    
    cmp bh,'0'
    cmp bl,'5'
    je hasil5
    
    cmp bh,'0'
    cmp bl,'6'
    je hasil6
    
    cmp bh,'0'
    cmp bl,'7'
    je hasil7
    
    cmp bh,'0'
    cmp bl,'8'
    je hasil8
    
    cmp bh,'0'
    cmp bl,'9'
    je hasil9
    
    cmp bh,'1'
    cmp bl,'0'
    je hasil10
    

    jne error_msg  
    
;-----------------------------------------------------------

hasil1:
    mov ah,09h
    lea dx,teks1
    int 21h
    int 20h

hasil2:
    mov ah,09h
    lea dx,teks2
    int 21h
    int 20h

hasil3:
    mov ah,09h
    lea dx,teks3
    int 21h
    int 20h

hasil4:
    mov ah,09h
    lea dx,teks4
    int 21h
    int 20h

hasil5:
    mov ah,09h
    lea dx,teks5
    int 21h
    int 20h

hasil6:
    mov ah,09h
    lea dx,teks6
    int 21h
    int 20h

hasil7:
    mov ah,09h
    lea dx,teks7
    int 21h
    int 20h

hasil8:
    mov ah,09h
    lea dx,teks8
    int 21h
    int 20h

hasil9:
    mov ah,09h
    lea dx,teks9
    int 21h
    int 20h

hasil10:
    mov ah,09h
    lea dx,teks10
    int 21h
    int 20h


    
;-----------------------------------------------------------------

teks1   db 13,10,'========================='
        db 13,10,'Pesanan anda adalah Ayam Goreng,mohon ditunggu','$'

teks2   db 13,10,'========================='
        db 13,10,'Pesanan anda adalah Ayam Bakar,mohon ditunggu','$'

teks3   db 13,10,'========================='
        db 13,10,'Pesanan anda adalah Nasi Goreng,mohon ditunggu','$'

teks4   db 13,10,'========================='
        db 13,10,'Pesanan anda adalah Sate,mohon ditunggu','$'

teks5   db 13,10,'========================='
        db 13,10,'Pesanan anda adalah Mi Goreng,mohon ditunggu','$'

teks6   db 13,10,'========================='
        db 13,10,'Pesanan anda adalah Kembang Tahu,mohon ditunggu','$'

teks7   db 13,10,'========================='
        db 13,10,'Pesanan anda adalah Rendang,mohon ditunggu','$'

teks8   db 13,10,'========================='
        db 13,10,'Pesanan anda adalah Gado-Gado,mohon ditunggu','$'

teks9   db 13,10,'========================='
        db 13,10,'Pesanan anda adalah Sup Buntut,mohon ditunggu','$'

teks10   db 13,10,'========================='
         db 13,10,'Pesanan anda adalah Soto Ayam,mohon ditunggu','$'
         
end start
        